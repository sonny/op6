#/bin/bash

count=0
while [ -z "$QMICLI_MODEM" ] && [ "$count" -lt "45" ]
do
        if qmicli --silent -pd qrtr://0 --uim-noop > /dev/null
        then
                QMICLI_MODEM="qmicli --silent -pd qrtr://0"
                echo "Using qrtr://0"
        fi
        sleep 1
        count=$((count+1))
done
echo "Waited $count seconds for modem device to appear"

QMI_CARDS=$($QMICLI_MODEM --uim-get-card-status)

# Extract first available slot number and AID for usim application
# on it. This should select proper slot out of two if only one UIM is
# present or select the first one if both slots have UIM's in them.
FIRST_PRESENT_SLOT=$(printf "%s" "$QMI_CARDS" | grep "Card state: 'present'" -m1 -B1 | head -n1 | cut -c7-7)
FIRST_PRESENT_AID=$(printf "%s" "$QMI_CARDS" | grep "usim (2)" -m1 -A3 | tail -n1 | awk '{print $1}')

echo "Selecting $FIRST_PRESENT_AID on slot $FIRST_PRESENT_SLOT"

# Finally send the new configuration to the modem.
$QMICLI_MODEM --uim-change-provisioning-session="slot=$FIRST_PRESENT_SLOT,activate=yes,session-type=primary-gw-provisioning,aid=$FIRST_PRESENT_AID" > /dev/null

