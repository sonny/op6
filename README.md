# op6

Development platform for GNOME mobile on OnePlus 6(T).
It is based on Arch Linux ARM (alarm).

This is intended for developers only. No end-user support will be provided.

Technical users are welcome to report issues and contribute.

⚠️ PLEASE READ CAREFULLY

- This will wipe your phone and all user data
- This will render Android unusable
- This may brick your phone.
- This is unstable and insecure software

## Goals

We welcome the diversity in the mobile Linux ecosystem but we want a development and demo target platform that is

- affordable
- accessible
- powerful
- standard
- upcycled

## Build the image

For now, see [instructions](./instructions.txt) for a rough set of commands to build your own image.

## Pre-install

1. [Upgrade to the latest version of OxygenOS](<https://wiki.postmarketos.org/wiki/OnePlus_6_(oneplus-enchilada)#Upgrade>) (OnePlus's version of Android).

2. [Unlock the bootloader](<https://wiki.postmarketos.org/wiki/OnePlus_6_(oneplus-enchilada)#Unlock_the_bootloader>)

## Install the image

[Enter Fastboot Mode](<https://wiki.postmarketos.org/wiki/OnePlus_6_(oneplus-enchilada)#How_to_enter_flash_mode>)

⚠️ Make sure you connected the right device.

```sh
fastboot erase dtbo
fastboot flash boot mainline-boot.img
fastboot flash userdata archlinux.img
fastboot reboot
```

You should be greated by GDM.

- username: `alarm`
- password: `123456`

## Post-install

Resize the root partition to take all the remaining space

```sh
sudo resize2fs /dev/sda17
```

Recommended settings

```sh
gsettings set org.gnome.mutter attach-modal-dialogs false
gsettings set org.gnome.desktop.interface clock-show-date false
# gsettings set org.gnome.desktop.wm.preferences button-layout ''
```

Recommended extensions

```sh
# Move clock away from the notch to the left
git clone https://gitlab.gnome.org/gnumdk/move-shell-clock.git ~/.local/share/gnome-shell/extensions/move-clock@gnumdk.org
# Add a rotate quick setting
git clone https://gitlab.gnome.org/gnumdk/add-rotate-button.git ~/.local/share/gnome-shell/extensions/add-rotate-button@gnumdk.org
# Add a display scale quick setting (for unoptimized apps)
wget https://extensions.gnome.org/extension-data/display-scale-switcherknokelmaat.gitlab.com.v4.shell-extension.zip
gnome-extensions install display-scale-switcherknokelmaat.gitlab.com.v4.shell-extension.zip
# Hide apps from grid (useful for demos)
https://extensions.gnome.org/extension/5895/app-hider/

# Logout

gnome-extensions enable move-clock@gnumdk.org
gnome-extensions enable add-rotate-button@gnumdk.org
gnome-extensions enable display-scale-switcher@knokelmaat.gitlab.com
```

Install Flatpak repositories

```sh
flatpak remote-delete --system flathub

flatpak remote-add --if-not-exists --user flathub https://flathub.org/repo/flathub.flatpakrepo
# flatpak remote-add --if-not-exists --user gnome-nightly https://nightly.gnome.org/gnome-nightly.flatpakrepo
# flatpak remote-add --if-not-exists --user flathub-beta https://flathub.org/beta-repo/flathub-beta.flatpakrepo
```

Apps

```sh
sudo pacman -S nautilus gnome-usage

flatpak install flathub org.gnome.clocks org.gnome.Contacts org.gnome.Maps org.gnome.Weather org.gnome.Calculator org.gnome.Calendar org.gnome.Characters org.gnome.TextEditor org.gnome.Logs org.gnome.baobab org.gnome.Evince io.bassi.Amberol org.gnome.Geary com.github.tchx84.Flatseal org.gabmus.whatip com.github.johnfactotum.Foliate org.gabmus.gfeeds com.belmoussaoui.Authenticator re.sonny.Tangram com.belmoussaoui.Decoder com.github.tenderowl.frog app.drey.Dialect org.gnome.TwentyFortyEight com.rafaelmardojai.Blanket com.mattjakeman.ExtensionManager org.gnome.Calls org.gnome.Snapshot org.gnome.Loupe org.gnome.Epiphany info.febvre.Komikku com.mattjakeman.ExtensionManager im.dino.Dino

# flatpak install gnome-nightly org.gnome.Fractal.Devel
```

Extra Apps

```sh
# KDE Connect implementation
flatpak --user install https://valent.andyholmes.ca/valent.flatpakref
```

## Troubleshooting

### Touch does not work

Your device may have a 3rd party touchscreen. See https://gitlab.com/msm8998-mainline/linux/-/issues/14

From a conversation with Caleb

> [found it](https://gitlab.com/msm8998-mainline/linux/-/issues/14#note_1220426301), this should work on op6 as well. you'd need to add the devicetree bits to sdm845-oneplus-common.dtsi and pick the other changes, it should make TS work.

## Bookmarks

- [GNOME Mobile chatroom](https://matrix.to/#/#mobile:gnome.org)
- [Snapdragon 845 chatroom](https://matrix.to/#/#sdm845:postmarketos.org)
- [Snapdragon 845 project](https://gitlab.com/sdm845-mainline)
- [postmarketOS OnePlus 6](<https://wiki.postmarketos.org/wiki/OnePlus_6_(oneplus-enchilada)>)
- [postmarketOS OnePlus 6T](<https://wiki.postmarketos.org/wiki/OnePlus_6T_(oneplus-fajita)>)
- [Manjaro support for op6](https://gitlab.manjaro.org/manjaro-arm/applications/arm-profiles/-/merge_requests/1)

- [LinuxPhoneApps](https://linuxphoneapps.org/apps/)
- [Mobian recommended applications](https://wiki.mobian-project.org/doku.php?id=apps)
- [postmarket recommended applications](https://wiki.postmarketos.org/wiki/Applications_by_category)
- [Purism List of Apps in Development](https://source.puri.sm/Librem5/community-wiki/-/wikis/List-of-Apps-in-Development)

## QA

### Why Arch Linux

Easy to put something together, has everything we need, "standard" desktop Linux components, bleeding edge packages, the aur is handy for testing development package.

### Why not postmarketOS

postmarketOS is a great project but it is unsuitable for a GNOME development platform

No systemd, uses busybox/musl libc, limited coredump support, ...

### Why Not Fedora

Inspired by this project, Fedora now has a bootable image [here](https://gitlab.com/fedora/sigs/mobility/sdm845/op6)

### Why not GNOME OS

https://gitlab.gnome.org/GNOME/gnome-build-meta/-/issues/623

We may consider dropping ArchLinux ARM in favor of GNOME OS in the future. In the meantime - we will gather here all the components and tweaks required to provide a great experience.

## Credits

Initial image and instructions put together by [Jonas Dreßler](https://gitlab.gnome.org/verdre/) with the help of [Caleb](https://floss.social/@calebccff@fosstodon.org).

Caleb and the postmarketOS community for supporting the OnePlus 6

Linaro for [mainlining the Qualcomm Snapdragon](https://www.linaro.org/blog/let-s-boot-the-mainline-linux-kernel-on-qualcomm-devices/) platform.

Purism for investing in the GNOME ecosystem.

Everyone else involved in making Linux mobile possible.
